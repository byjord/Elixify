# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :elixify,
  ecto_repos: [Elixify.Repo]

# General application configuration
config :elixify, :hex_url,
  repo: "https://repo.hex.pm/",
  api: "https://hex.pm/api/"

# Configures the endpoint
config :elixify, ElixifyWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "MoFdTOBxvp1oBWE44bEnyV74EhQTeR+WvRutkSGQjHQA1Xha+X/etMLesY1mZSek",
  render_errors: [view: ElixifyWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Elixify.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
