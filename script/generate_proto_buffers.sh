#!/bin/bash

PROTOS=("
  priv/static/hexpm/specifications/names.proto
  priv/static/hexpm/specifications/package.proto
  priv/static/hexpm/specifications/signed.proto
  priv/static/hexpm/specifications/versions.proto
")

rm -rf ./lib/hexpm/protoc/*

for file in $PROTOS; do
  protoc -I ./priv/static/hexpm/specifications/ --elixir_out=plugins=grpc:./lib/hexpm/protoc $file
done
