#!/bin/bash -ex

for pkg in elixir node yarn ffmpeg; do
  if brew list -1 | grep -q "^${pkg}\$"; then
    echo "Package '$pkg' is installed, skipping.."
  else
    echo "brew install $pkg"
    brew install $pkg
  fi
done

set -x

cd assets && yarn install
