defmodule ElixifyWeb.PageController do
  use ElixifyWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
