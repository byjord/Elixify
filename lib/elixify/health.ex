defmodule Elixify.HealthChecks do
  def check_app, do: :ok
  def check_db, do: :ok
  def check_redis, do: :ok
end
