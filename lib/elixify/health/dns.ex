defmodule Elixify.Health.DNS do
  @important [
    __MODULE__.Resolve.shopifycom(),
    __MODULE__.Resolve.googlecom(),
    __MODULE__.Resolve.facebook(),
    __MODULE__.Resolve.youtube(),
    __MODULE__.Resolve.lloydsbank(),
    __MODULE__.Resolve.chase()
  ]

  @baseline [
    __MODULE__.Resolve.shopifycouk(),
    __MODULE__.Resolve.shopifyca(),
    __MODULE__.Resolve.googlecouk(),
    __MODULE__.Resolve.googleca(),
    __MODULE__.Resolve.taobao(),
    __MODULE__.Resolve.tmall(),
    __MODULE__.Resolve.pornhub(),
    __MODULE__.Resolve.vk(),
    __MODULE__.Resolve.reddit(),
    __MODULE__.Resolve.qq(),
    __MODULE__.Resolve.imgur()
  ]

  def test(:required), do: {:ok, @important}
  def test(:normal), do: {:ok, @baseline}

  defmodule Resolve do
    def shopifycom, do: {:ok, _} = DNS.resolve("shopify.com")
    def shopifycouk, do: {:ok, _} = DNS.resolve("shopify.co.uk")
    def shopifyca, do: {:ok, _} = DNS.resolve("shopify.ca")

    def googlecom, do: {:ok, _} = DNS.resolve("google.com")
    def googlecom, do: {:ok, _} = DNS.resolve("google.co.in")
    def googlecouk, do: {:ok, _} = DNS.resolve("google.co.uk")
    def googleca, do: {:ok, _} = DNS.resolve("google.ca")

    def facebook, do: {:ok, _} = DNS.resolve("facebook.com")

    # From the Alexa top 500
    # Using lists: Global, UK, USA, Russia, Denmark, Luxembourg, and Romania
    def youtube, do: {:ok, _} = DNS.resolve("youtube.com")
    def baidu, do: {:ok, _} = DNS.resolve("baidu.com")
    def wikipedia, do: {:ok, _} = DNS.resolve("wikipedia.com")
    def yahoo, do: {:ok, _} = DNS.resolve("yahoo.com")
    def qq, do: {:ok, _} = DNS.resolve("qq.com")
    def taobao, do: {:ok, _} = DNS.resolve("taobao.com")
    def tmall, do: {:ok, _} = DNS.resolve("tmall.com")
    def amazon, do: {:ok, _} = DNS.resolve("amazon.com")
    def instagram, do: {:ok, _} = DNS.resolve("instagram.com")
    def vk, do: {:ok, _} = DNS.resolve("vk.com")
    def reddit, do: {:ok, _} = DNS.resolve("reddit.com")
    def sohu, do: {:ok, _} = DNS.resolve("sohu.com")
    def yandex, do: {:ok, _} = DNS.resolve("yandex.ru")
    def netflix, do: {:ok, _} = DNS.resolve("netflix.com")
    def pornhub, do: {:ok, _} = DNS.resolve("pornhub.com")
    def alipay, do: {:ok, _} = DNS.resolve("alipay.com")
    def mailru, do: {:ok, _} = DNS.resolve("mail.ru")
    def xvideos, do: {:ok, _} = DNS.resolve("xvideos.com")
    def ebay, do: {:ok, _} = DNS.resolve("ebay.com")
    def jd, do: {:ok, _} = DNS.resolve("jd.com")
    def imgur, do: {:ok, _} = DNS.resolve("imgur.com")
    def pinterest, do: {:ok, _} = DNS.resolve("pinterest.com")
    def apple, do: {:ok, _} = DNS.resolve("apple.com")
    def barclays, do: {:ok, _} = DNS.resolve("barclays.co.uk")
    def homebarclays, do: {:ok, _} = DNS.resolve("home.barclays")
    def lloydsbank, do: {:ok, _} = DNS.resolve("lloydsbank.co.uk")
    def bankofamerica, do: {:ok, _} = DNS.resolve("bankofamerica.com")
    def wellsfargo, do: {:ok, _} = DNS.resolve("wellsfargo.com")
    def chase, do: {:ok, _} = DNS.resolve("chase.com")
    def livejasmin, do: {:ok, _} = DNS.resolve("livejasmin.com")
    def quora, do: {:ok, _} = DNS.resolve("quora.com")

    def debr, do: {:ok, _} = DNS.resolve("debr.dk")
    def ekstrabladet, do: {:ok, _} = DNS.resolve("ekstrabladet.dk")
    def dba, do: {:ok, _} = DNS.resolve("dba.dk")
    def bt, do: {:ok, _} = DNS.resolve("bt.dk")
    def tv2, do: {:ok, _} = DNS.resolve("tv2.dk")
    def emu, do: {:ok, _} = DNS.resolve("emu.dk")
    def dtu, do: {:ok, _} = DNS.resolve("dtu.dk")
    def politiken, do: {:ok, _} = DNS.resolve("politiken.dk")
    def txxx, do: {:ok, _} = DNS.resolve("txxx.com")

    def rtl, do: {:ok, _} = DNS.resolve("rtl.lu")
    def public, do: {:ok, _} = DNS.resolve("public.lu")
    def snet, do: {:ok, _} = DNS.resolve("snet.lu")
    def wort, do: {:ok, _} = DNS.resolve("wort.lu")
    def education, do: {:ok, _} = DNS.resolve("education.lu")
    def lessentiel, do: {:ok, _} = DNS.resolve("lessentiel.lu")
    def bil, do: {:ok, _} = DNS.resolve("bil.com")
    def post, do: {:ok, _} = DNS.resolve("post.lu")
    def ccp, do: {:ok, _} = DNS.resolve("ccp-connect.lu")
    def athome, do: {:ok, _} = DNS.resolve("athome.lu")

    def avito, do: {:ok, _} = DNS.resolve("Avito.ru")
    def okru, do: {:ok, _} = DNS.resolve("ok.ru")
    def rambler, do: {:ok, _} = DNS.resolve("rambler.ru")
    def sberbank, do: {:ok, _} = DNS.resolve("sberbank.ru")
    def gismeteo, do: {:ok, _} = DNS.resolve("gismeteo.ru")
    def gosuslugi, do: {:ok, _} = DNS.resolve("gosuslugi.ru")
    def drom, do: {:ok, _} = DNS.resolve("drom.ru")
    def kinopoisk, do: {:ok, _} = DNS.resolve("kinopoisk.ru")
    def userapi, do: {:ok, _} = DNS.resolve("userapi.com")
    def ria, do: {:ok, _} = DNS.resolve("ria.ru")
    def pikabu, do: {:ok, _} = DNS.resolve("pikabu.ru")
    def infourok, do: {:ok, _} = DNS.resolve("infourok.ru")
    def wildberries, do: {:ok, _} = DNS.resolve("wildberries.ru")
    def hh, do: {:ok, _} = DNS.resolve("hh.ru")
    def drive2, do: {:ok, _} = DNS.resolve("drive2.ru")
    def porn555, do: {:ok, _} = DNS.resolve("porn555.com")
    def fbru, do: {:ok, _} = DNS.resolve("fb.ru")
    def ozon, do: {:ok, _} = DNS.resolve("ozon.ru")
    def rbc, do: {:ok, _} = DNS.resolve("rbc.ru")
    def rutube, do: {:ok, _} = DNS.resolve("rutube.ru")
    def lenta, do: {:ok, _} = DNS.resolve("lenta.ru")
    def kp, do: {:ok, _} = DNS.resolve("kp.ru")
    def yadi, do: {:ok, _} = DNS.resolve("yadi.sk")
  end
end
