# Elixify

1.  Setup `$ ./script/setup.sh` or separately:
    1.  Assets/Node `$ ./script/assets_setup.sh`
    2.  Db `$ ./script/db_setup.sh`
    3.  Deps `$ ./script/deps_setup.sh`
    4.  Protobufs `$ ./script/generate_proto_buffers.sh`
2.  Clean up, removes generated files
    1.  Elixir `$ ./script/clean_elixir.sh`
    2.  Assets/Node `$ ./script/clean_assets.sh`
